# -*- coding: utf-8 -*-
import logging
import os

import httplib2
from apiclient.discovery import build
from flask import render_template, jsonify, Blueprint
from google.appengine.api import memcache
from oauth2client.contrib.appengine import AppAssertionCredentials
from oauth2client.service_account import ServiceAccountCredentials

from settings import SERVICE_ACCOUNT_JSON

bp_hello = Blueprint('hello', __name__, url_prefix='/hello')


@bp_hello.route('/', methods=['GET'])
def home():
    return render_template('index.html')


@bp_hello.route('/calendar', methods=['GET'])
def calendar():
    if os.environ.get('SERVER_SOFTWARE', 'Dev').startswith('Dev'):
        scopes = ['https://www.googleapis.com/auth/calendar']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            SERVICE_ACCOUNT_JSON, scopes)
    else:
        credentials = AppAssertionCredentials(scope='https://www.googleapis.com/auth/calendar')

    calendar_id = '<calendar-id>'
    http = credentials.authorize(httplib2.Http(memcache))
    service = build('calendar', 'v3')
    events = service.events().list(
        calendarId=calendar_id,
        singleEvents=True, maxResults=250,
        orderBy='startTime').execute(http=http)

    calendars = service.calendarList().list().execute(http)
    logging.info(calendars)
    return jsonify(events)
